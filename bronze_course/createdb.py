from nbgrader.apps import NbGraderAPI
from traitlets.config import Config

config = Config()


api = NbGraderAPI(config=config)


for a in api.get_source_assignments():
    api.gradebook.add_assignment(a)


