# autograded-basics-of-quantum-computing

This project contains [QBronze notebooks](https://gitlab.com/qkitchen/basics-of-quantum-computing) that can be graded automatically using [nbgrader](https://github.com/jupyter/nbgrader), or otherwise. The purpose of autograding is two fold:

- Help students identify if they have successfully completed the coding task.
- Help quickly and cheaply certify students for the QBronze diploma.

The goals of this project are: 
- Write tests for the tasks contained in the QBronze notebooks.
- If necessary, suggest changes to the tasks so they can autograded better.

# Preparing your environment
We use the nbgrader package to write our autograded tests. This only works with the Jupyter notebook environment, and not the JupterLab one. Follow the following steps so you can start reading and writing tests.

1. Install Jupyter notebook. I recommend using [Anaconda](https://www.anaconda.com/) which comes pre-installed with it, and makes it easy to manage your python packages.
2. Follow the nbgrader [installation instructions](https://nbgrader.readthedocs.io/en/stable/user_guide/installation.html). Install the nbgrader python package, as well as all the Jupyter notebook extensions provided.
3. Clone this repository, if you have not already, in a subdirectory of where you run the Jupyter notebook.
4. Next, you want to set up the a nbgrader course on your local machine. First find the config directory of your Jupyter notebook installation using `jupyter --paths`. Copy the `nbgrader_config.py` file from this repository to Jupyter notebook config directory.  Edit the copied `nbgrader_config.py` and set the `c.CourseDirectory.root` variable to the `bronze_course` directory found in this repository. Further details can be found [here](https://nbgrader.readthedocs.io/en/stable/configuration/nbgrader_config.html).
5. In order to use the nbgrader formgrader extension, create a directory called `exchange` somewhere on your computer. Set the `c.Exchange.root` variable in the `nbgrader_config.py` file to this folder. Further details can be found [here](https://nbgrader.readthedocs.io/en/stable/user_guide/managing_assignment_files.html#setting-up-the-exchange). Make sure you set permissions to the exchange directory as per this link.
6. Run `bronze_course/createdb.py` to create your local sqlite database that nbgrader uses to store course data.
7. Launch Jupyter notebook. You should see "Formgrader" as one of the tabs. Clicking on it, will open a new tab with a list of assignments, and no errors. If you get an error, recheck your paths.
8. Read the nbgrader [philosphy](https://nbgrader.readthedocs.io/en/stable/user_guide/philosophy.html) and basic [workflow](https://nbgrader.readthedocs.io/en/stable/user_guide/creating_and_grading_assignments.html) to understand how everything works.

# Creating assignments and writing tests

Each notebook in the Bronze notebook will have its own assignment within our nbgrader course. The name of each assignment will be the same as the notebook, postpended by "_Assignment". From Formgrader click on one of the pre-existing assignments and then open the notebook within it. Select "View"->"Cell Toolbar"->"Create Assignment" to load the assignment editing features.

Code cells marked as "Autograded answers" are to be filled by the students. The instructor version of the assignment contains a sample solution in these cells. Code cells marked as "Autograded tests" grade student answers. If a test cell executes without errors, students gain full points for that cell. Add, edit or remove tests as you feel necessary. 

The wording of most tasks will have to be tweaked in order to turn them into one that can be autograded. Please try to not make too many drastic changes, though.

This repository contains a copy of the [QBronze repository](https://nbgrader.readthedocs.io/en/stable/user_guide/creating_and_grading_assignments.html) from 20 December 2020. If you want to work on a new notebook, create a new assignment, name it as described above, and copy the notebook to the assignment.

# How to autograde well

Our goal is to create tasks and tests that maximize student understanding of the workshop material. This is unlike typical university courses, where assessments are usually summative; meaning their goals is to _evaluate_ student learning. Our main goal is to help the student learn better, and therefore our assessment methodology must be largely formative: meaning their goal is to give students feedback on their progress, as well as identify gaps in their solutions.

In the long run, the notebooks should evolve so that the tasks and tests mesh well, and maximize student learning. However, in the short term, we should aim to keep the notebook content as static as possible.

# Technical issues with autograding

- The standard way of using nbgrader requires that every solution is a function. The way many tasks are written, this may not be feasible. We have to discover ways of using nbgrader to autograde "scripts".

- A lot of task solutions have random output. Random outputs are difficult to write tests for. 

# License

The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode.

The code snippets in the notebooks are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.
