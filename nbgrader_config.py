c = get_config()

c.CourseDirectory.course_id = "bronze_course"

# Root directory of course. 
# Should be the bronze_course folder in this repo.
# Must be a subdirectory of where you run Jupyter notebook.
c.CourseDirectory.root = ""

# Exchange server. Create it anywhere on your machine. 
c.Exchange.root = ""



